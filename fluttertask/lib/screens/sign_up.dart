import 'package:flutter/material.dart';
import 'package:fluttertask/constant/colors.dart';
import 'package:fluttertask/model/services/register.dart';
import 'package:fluttertask/screens/homepage.dart';
import 'package:fluttertask/screens/nav.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final formKey = new GlobalKey<FormState>();

  String? _email, _fullName, _password;

  @override
  Widget build(BuildContext context) {
    final EmailField = TextFormField(
      autofocus: false,
      maxLines: 1,
      minLines: 1,
      onSaved: (value) => _email = value!,
      decoration: InputDecoration(
        labelText: 'Fullname',
        labelStyle: TextStyle(color: Colors.black),
        contentPadding: new EdgeInsets.symmetric(vertical: 0, horizontal: 1.0),
        // enabledBorder:
        //     OutlineInputBorder(borderSide: new BorderSide(color: Colors.black)),
        // focusedBorder: OutlineInputBorder(
        //     borderSide:
        //         new BorderSide(color: Colors.black, style: BorderStyle.solid)),
      ),
    );

    final fullNameField = TextFormField(
      autofocus: false,
      maxLines: 1,
      onSaved: (value) => _fullName = value!,
      minLines: 1,
      decoration: InputDecoration(
        labelText: 'E-mail',
        labelStyle: TextStyle(color: Colors.black),
        fillColor: Colors.amber,
        focusColor: Colors.amber,
        contentPadding: new EdgeInsets.symmetric(vertical: 0, horizontal: 1.0),
        // enabledBorder:
        //     OutlineInputBorder(borderSide: new BorderSide(color: Colors.black)),
        // focusedBorder: OutlineInputBorder(
        //     borderSide:
        //         new BorderSide(color: Colors.black, style: BorderStyle.solid)),
      ),
    );
    final passwordField = TextFormField(
      autofocus: false,
      obscureText: true,
      validator: (value) => value!.isEmpty ? "Please enter password" : null,
      maxLines: 1,
      minLines: 1,
      onSaved: (value) => _password = value,
      decoration: InputDecoration(
        labelText: 'password',
        labelStyle: TextStyle(color: Colors.black),
        contentPadding: new EdgeInsets.symmetric(vertical: 0, horizontal: 1.0),
        // enabledBorder:
        //     OutlineInputBorder(borderSide: new BorderSide(color: Colors.black)),
        // focusedBorder: OutlineInputBorder(/////////
        // borderSide:
        //     new BorderSide(color: Colors.black, style: BorderStyle.solid)),
      ),
    );
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(Icons.arrow_back),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "Let's start here",
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w700,
                      fontSize: 34),
                ),
                Text(
                  'Fill in your details to begin',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w500,
                      fontSize: 17),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  height: 60,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14),
                      color: Colors.grey[300]),
                  padding: EdgeInsets.only(top: 10),
                  child: Column(
                    children: [
                      EmailField,
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 60,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14),
                      color: Colors.grey[300]),
                  padding: EdgeInsets.only(top: 10),
                  child: Column(
                    children: [
                      fullNameField,
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 60,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14),
                      color: Colors.grey[300]),
                  padding: EdgeInsets.only(top: 10),
                  child: Column(
                    children: [
                      passwordField,
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MyStatefulWidget()));
                  },
                  height: 60,
                  child: Text(
                    'Sign up',
                    style: TextStyle(
                        fontSize: 17,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w700),
                  ),
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      side: BorderSide(
                          color: kPColor, width: 1, style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(10)),
                  color: kPColor,
                  minWidth: width,
                ),
                SizedBox(
                  height: height / 5,
                ),
                Center(
                  child: Text(
                      'By signing in, I agree with Terms of use\n                 and Privacy policty',
                      style: TextStyle(
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w500,
                        fontSize: 13,
                      )
                      // RichText(text: TextSpan(
                      //     text: 'By signing in, I agree with',
                      //   children: <TextSpan>[

                      //     TextSpan(text: 'Terms of use', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 13)),
                      //     TextSpan(text: 'and'),
                      //     TextSpan(text: 'Privacy Policy',style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 13))
                      //     ]
                      // ))
                      ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
