import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertask/constant/colors.dart';
import 'package:fluttertask/screens/sign_up.dart';


class OnboardingScreen extends StatelessWidget {
  const OnboardingScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: height,
            width: width,
            
             decoration: BoxDecoration(
              color: kPrimaryColor[20],   
              image: DecorationImage(image: AssetImage('assets/images/onbaord.png',), fit: BoxFit.cover)
            ),
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      // SvgPicture.asset('assets/images/logo.png'),
                      Image.asset('assets/images/aa.png'),
                      Text('WOO\nDOG', style: TextStyle(color: kPrimaryColor, fontFamily: 'Poppins', fontWeight: FontWeight.w900),)
                      
                    ],
                  ),
                  SizedBox(height: height/2,),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(backgroundColor: Colors.white, child: Text('1', style: TextStyle(color: Colors.black),), radius: 15,),
                      Text('-'),
                       CircleAvatar(backgroundColor: Colors.black, child: Text('2', style: TextStyle(color: Colors.white),), radius: 15,),
                       Text('-'),
                        CircleAvatar(backgroundColor: Colors.black, child: Text('3'), radius: 15,)
                    ],
                  ),
                  SizedBox(height: 20,),
                  Text("Too tired to walk your dog? \n            Let's help you!", style: TextStyle(color: Colors.white, fontFamily: 'Poppins', fontSize: 22,fontWeight: FontWeight.w700),),
                    SizedBox(height: height/20,),
                   FlatButton(
                    onPressed: () {
                     Navigator.push(context, MaterialPageRoute(builder: (context) =>SignUpScreen()));
                    },
                    height: 60,
                    child: Text(
                      'Join the community',
                      style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w500),
                    ),
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: kPColor,
                            width: 1,
                            style: BorderStyle.solid),
                              borderRadius: BorderRadius.circular(10)
                       ),
                    color: kPColor,
                    minWidth: width,
                  ),
                    SizedBox(height: height/30,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Already a member?  ', style: TextStyle(color: Colors.white),),
                      Text('Sign in', style: TextStyle(color: kPrimaryColor),)
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
    );
  }
}