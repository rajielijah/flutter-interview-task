import 'package:flutter/material.dart';
import 'package:fluttertask/constant/colors.dart';
import 'package:fluttertask/screens/chat.dart';

class HomePage extends StatelessWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
              ListTile(
                title: Text('Home', style: TextStyle(fontWeight: FontWeight.w700, fontFamily: 'Poppins', fontSize: 34),),
                subtitle: Text('Explore dog walker', style: TextStyle(fontFamily: 'Poppins',  fontWeight: FontWeight.w500,fontSize: 17),),
                trailing: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ChatScreen()));
                  },
                  child: Container(
                    width: 130,
                    height: 41,
                    decoration: BoxDecoration(
                      color: kPColor,
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Row(
                      children: [
                        Icon(Icons.add, color: Colors.white,),
                        Text('Book a walk', style: TextStyle(fontFamily: 'Poppins', color: Colors.white),)
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Container(
                  width: width,
                  height: 49.5,
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: ListTile(
                    leading: Icon(Icons.location_on_outlined),
                    title: Text('Kiyv, Ukraine', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w400, fontSize: 17),),
                    horizontalTitleGap: -10,
                    trailing: Icon(Icons.import_export_outlined),
                    minVerticalPadding: -10,
                  ),
                ),
              ),
              ListTile(
                title: Text('Near you', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w700, fontSize: 34),),
                trailing: Text('view all', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w400, fontSize: 15, decoration: TextDecoration.underline,),),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Column(
                        children: [
                          Image.asset('assets/images/frame2.png'),
                         Row(
                           mainAxisAlignment:MainAxisAlignment.start,
                           children: [
                             Column(
                               mainAxisAlignment: MainAxisAlignment.start,
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: [
                                 Text('Mason York', style: TextStyle(fontFamily: 'Poppins', fontSize: 17, fontWeight: FontWeight.w500),),
                                  Text('7 km from you',  style: TextStyle(fontFamily: 'Poppins', fontSize: 10, fontWeight: FontWeight.w500))
                               ],
                             ),
                             SizedBox(width: 25,),
                             Container(
                    width: 45,
                    height: 25,
                    decoration: BoxDecoration(
                      color:kPrimaryColor[20],
                      borderRadius: BorderRadius.circular(10)
                    ),
                      child: Center(child: Text('\$3/h', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 10, color: Colors.white),)),
                    )
                           ],
                         )
                        ],
                      ),
                      SizedBox(width: 30,),
                       Column(
                        children: [
                          Image.asset('assets/images/frame1.png'),
                         Row(
                           mainAxisAlignment:MainAxisAlignment.start,
                           children: [
                             Column(
                               mainAxisAlignment: MainAxisAlignment.start,
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: [
                                 Text('Mason York', style: TextStyle(fontFamily: 'Poppins', fontSize: 17, fontWeight: FontWeight.w500),),
                                  Text('7 km from you',  style: TextStyle(fontFamily: 'Poppins', fontSize: 10, fontWeight: FontWeight.w500))
                               ],
                             ),
                             SizedBox(width: 25,),
                             Container(
                    width: 45,
                    height: 25,
                    decoration: BoxDecoration(
                      color:kPrimaryColor[20],
                      borderRadius: BorderRadius.circular(10)
                    ),
                      child: Center(child: Text('\$3/h', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 10, color: Colors.white),)),
                    )
                           ],
                         )
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Divider(thickness: 2,),
                   ListTile(
                title: Text('Suggested', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w700, fontSize: 34),),
                trailing: Text('view all', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w400, fontSize: 15, decoration: TextDecoration.underline,),),
              ),
               Padding(
                padding: const EdgeInsets.all(18.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Column(
                        children: [
                          Image.asset('assets/images/frame1.png'),
                         Row(
                           mainAxisAlignment:MainAxisAlignment.start,
                           children: [
                             Column(
                               mainAxisAlignment: MainAxisAlignment.start,
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: [
                                 Text('Mason York', style: TextStyle(fontFamily: 'Poppins', fontSize: 17, fontWeight: FontWeight.w500),),
                                  Text('7 km from you',  style: TextStyle(fontFamily: 'Poppins', fontSize: 10, fontWeight: FontWeight.w500))
                               ],
                             ),
                             SizedBox(width: 25,),
                             Container(
                    width: 45,
                    height: 25,
                    decoration: BoxDecoration(
                      color:kPrimaryColor[20],
                      borderRadius: BorderRadius.circular(10)
                    ),
                      child: Center(child: Text('\$3/h', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 10, color: Colors.white),)),
                    )
                           ],
                         )
                        ],
                      ),
                      SizedBox(width: 30,),
                       Column(
                        children: [
                          Image.asset('assets/images/frame2.png'),
                         Row(
                           mainAxisAlignment:MainAxisAlignment.start,
                           children: [
                             Column(
                               mainAxisAlignment: MainAxisAlignment.start,
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: [
                                 Text('Mason York', style: TextStyle(fontFamily: 'Poppins', fontSize: 17, fontWeight: FontWeight.w500),),
                                  Text('7 km from you',  style: TextStyle(fontFamily: 'Poppins', fontSize: 10, fontWeight: FontWeight.w500))
                               ],
                             ),
                             SizedBox(width: 25,),
                             Container(
                    width: 45,
                    height: 25,
                    decoration: BoxDecoration(
                      color:kPrimaryColor[20],
                      borderRadius: BorderRadius.circular(10)
                    ),
                      child: Center(child: Text('\$3/h', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 10, color: Colors.white),)),
                    )
                           ],
                         )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],),
          ),
        ),
      ),
    );
  }
}