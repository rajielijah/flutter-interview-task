import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertask/screens/chatbox.dart';
import 'package:fluttertask/screens/homepage.dart';


class ChatScreen extends StatelessWidget {
  const ChatScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
     var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        body: SafeArea(child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top:18.0, left:18, right: 18),
                child: Text('Chat', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w700, fontSize: 34),),
              ),
                 Padding(
                padding: const EdgeInsets.all(18.0),
                child: Container(
                  width: width,
                  height: 49.5,
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: ListTile(
                    leading: Icon(Icons.search),
                    title: Text('Search..', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w400, fontSize: 17),),
                    horizontalTitleGap: -10,
                    trailing: Icon(Icons.import_export_outlined),
                    minVerticalPadding: -10,
                  ),
                ),
              ),
              Divider(thickness: 2,),
              GestureDetector(
                onTap: (){

                  Navigator.push(context, MaterialPageRoute(builder: (context) =>ChatBoxScreen()));
                },
                child: ListTile(
                  leading: Image.asset('assets/images/chat4.png'),
                  title: Text('Will Knowles', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w700, fontSize: 20),),
                  subtitle: Text('Hey! How’s your dog? ∙ 10mins', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 17)),
                  trailing: CircleAvatar(backgroundColor: Colors.amber, radius: 3,),
                ),
              ),
              Divider(),
              ListTile(
                leading: Image.asset('assets/images/chat.png'),
                title: Text('Matt Chapman', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w700, fontSize: 20),),
                subtitle: Text('Hey! How’s your dog? ∙ 1h', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 17)),
                // trailing: CircleAvatar(backgroundColor: Colors.amber, radius: 3,),
              ),
              Divider(),
              ListTile(
                leading: Image.asset('assets/images/chat1.png'),
                title: Text('Hazel Redd', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w700, fontSize: 20),),
                subtitle: Text('Hey! Long time no see ∙ 2h', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 17)),
                trailing: CircleAvatar(backgroundColor: Colors.amber, radius: 3,),
              ),
              Divider(),
              ListTile(
                leading: Image.asset('assets/images/chat2.png'),
                title: Text('Michael Todd', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w700, fontSize: 20),),
                subtitle: Text('How are you doing', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 17)),
                // trailing: CircleAvatar(backgroundColor: Colors.amber, radius: 3,),
              ),
              Divider(),
              ListTile(
                leading: Image.asset('assets/images/chat3.png'),
                title: Text('Will Johnson', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w700, fontSize: 20),),
                subtitle: Text('Hey! How’s your dog? ∙ 5h', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 17)),
                // trailing: CircleAvatar(backgroundColor: Colors.amber, radius: 3,),
              ),
              Divider(),
              ListTile(
                leading: Image.asset('assets/images/chat4.png'),
                title: Text('Will Knowles', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w700, fontSize: 20),),
                subtitle: Text('Hey! How’s your dog? ∙ 8h', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 17)),
                trailing: CircleAvatar(backgroundColor: Colors.amber, radius: 3,),
              ),
              Divider(),
            ],
          ),
        ),),
    );
  }
}