

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertask/constant/colors.dart';
import 'package:fluttertask/screens/homepage.dart';

class MyStatefulWidget extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  // static const color = const Color(0xFF184C35);
  static const color  = kPrimaryColor;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    HomePage(),
    HomePage(),
HomePage(),
    // Profile(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


@override
  Widget build(BuildContext context) {
    final String assetName = 'images/vector1.svg';
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items:  <BottomNavigationBarItem>[
           BottomNavigationBarItem(
           icon: SvgPicture.asset('assets/images/Home.svg'),
            label: 'Home',
            backgroundColor: Colors.black,
          ),
          BottomNavigationBarItem(
            label: 'Moments',
            backgroundColor: color,
           icon: Icon(Icons.people_outline)
            // icon: ImageIcon(AssetImage('images/trade.png'))
          ),
          BottomNavigationBarItem(
          icon: SvgPicture.asset('assets/images/Send.svg'),
           label: 'Chat',
           backgroundColor: color,
          ),
          BottomNavigationBarItem(
           icon: Icon(Icons.person_outline),
            label: 'Send',
            backgroundColor: color,
          ),
        
        ],
        type: BottomNavigationBarType.fixed,
        // backgroundColor: kPColor,
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.black87,
        selectedIconTheme: IconThemeData(color: Colors.black),
        unselectedItemColor: Colors.grey[500],
        onTap: _onItemTapped,
        iconSize: 30,
        // elevation: 30,
      ),
    );
  }
}