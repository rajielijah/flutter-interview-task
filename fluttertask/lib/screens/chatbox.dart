import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ChatBoxScreen extends StatelessWidget {
  const ChatBoxScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
     var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        body: SafeArea(
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Icon(Icons.arrow_back),
                    SizedBox(width: width/15,),
                    Image.asset('assets/images/chat1.png'),
                    SizedBox(width: width/25,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Alex Murray', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w700, fontSize: 17),),
                        Text('Online', style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 13),),
                       
                      ],
                    ),
                    SizedBox(width: width/4,),
                    Icon(Icons.phone),
                  ],
                )
              ],
            ),
          ),
        ),
    );
  }
}