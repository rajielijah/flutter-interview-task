import 'package:flutter/material.dart';

const MaterialColor kPrimaryColor = const MaterialColor(
  0xFFE73A40,
  const <int, Color> {
    10: const Color(0xFFFE904B),
    20: const Color(0xFF2B2B2B),
    30: const Color(0xFFFB724C)
  }
);

const MaterialColor kPColor = const MaterialColor(
  0xFFFB724C,
  const <int, Color> {
    10: const Color(0xFFFE904B),
  }
);