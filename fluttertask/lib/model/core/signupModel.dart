import 'dart:convert';

Register registerFromJson(String str) => Register.fromJson(json.decode(str));

String registerToJson(Register data) => json.encode(data.toJson());

class Register {
    Register({
        required this.fullName,
        this.lastName,
        this.email,
        this.password,
        });

    String fullName;
    String email;
    String password;
   

    factory Register.fromJson(Map<String, dynamic> json) => Register(
        fullName: json["firstName"],
        // lastName: json["lastName"],
        email: json["email"],
        password: json["password"],
    );

    Map<String, dynamic> toJson() => {
        "fullName": fullName,
        // "lastName": lastName,
        "email": email,
        "password": password,
    };
}
